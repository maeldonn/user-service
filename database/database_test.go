package database

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_New(t *testing.T) {
	db := New()

	assert.Len(t, db.DB, numberOfUsers)
	for i := range db.DB {
		assert.NotEmpty(t, &db.DB[i])
	}
}
