package database

import (
	"fmt"
	"math/rand"
	"users/common/genproto/users"
	"users/utils"
)

const numberOfUsers = 10_000

type UserDB struct {
	DB []users.User
}

func New() *UserDB {
	db := make([]users.User, 0, numberOfUsers)

	for i := range numberOfUsers {
		db = append(db, users.User{
			ID:      int32(i),
			FName:   fmt.Sprintf("name_%d", i),
			City:    utils.RandomCity(),
			Phone:   utils.RandomPhoneNumber(),
			Height:  utils.RandomRange(4.6, 6.9),
			Married: rand.Intn(2) == 1,
		})
	}

	return &UserDB{
		DB: db,
	}
}
