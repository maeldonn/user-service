package main

import (
	"fmt"
	"log"
	"net"
	"users/common/genproto/users"
	"users/services"

	"google.golang.org/grpc"
)

func main() {
	var (
		server      = grpc.NewServer()
		userService = services.NewUser()
		port        = fmt.Sprintf(":%d", 8081)
	)

	users.RegisterUserServiceServer(server, userService)

	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("cannot start server on port 8081: %v", err)
	}

	log.Printf("Starting gRPC server on %s", port)
	if err := server.Serve(lis); err != nil {
		log.Fatalf("An error occured: %v", err)
	}
}
