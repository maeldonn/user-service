package utils

import (
	"math/rand"
	"strconv"
)

var cities = [...]string{
	"Paris",
	"Berlin",
	"Dublin",
	"New York",
	"Washington",
	"San Francisco",
}

func RandomCity() string {
	return cities[rand.Intn(len(cities))]
}

func RandomPhoneNumber() string {
	var number string
	for range 10 {
		n := rand.Intn(10)
		number += strconv.Itoa(n)
	}
	return number
}

func RandomRange(min, max float32) float32 {
	return min + rand.Float32()*(max-min)
}
