package utils

import (
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_RandomCity(t *testing.T) {
	for range 100 {
		city := RandomCity()
		assert.Contains(t, cities, city)
	}
}

func Test_RandomNumber(t *testing.T) {
	regexp := regexp.MustCompile(`^\d{10}$`)
	for range 100 {
		phoneNumber := RandomPhoneNumber()
		assert.Regexp(t, regexp, phoneNumber)
	}
}

func Test_RandomRange(t *testing.T) {
	var min, max float32 = 4.2, 6.5
	for range 100 {
		n := RandomRange(min, max)
		assert.GreaterOrEqual(t, n, min)
		assert.LessOrEqual(t, n, max)
	}
}
