all: run

gen:
	protoc \
	  --proto_path=protobuf "protobuf/users.proto" \
	  --go_out=common/genproto/users \
	  --go_opt=paths=source_relative \
  	  --go-grpc_out=common/genproto/users \
	  --go-grpc_opt=paths=source_relative

run: gen
	go run cmd/users/main.go

docker: gen
	docker build -t users .
	docker run --name users -p 8081:8081 -d users

test: gen
	go test ./...

lint: gen
	golangci-lint run

.PHONY: run gen docker test lint
.SILENT: run gen docker test lint