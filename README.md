# User service

_A gRPC user service_

## How to run

### Basic

```
$ make run
```

### Docker

```
$ make docker
```

## How to dev

```
$ make gen
```

```
$ go mod tidy
```

## How to test

```
$ make test
```