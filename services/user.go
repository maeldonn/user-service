package services

import (
	"context"
	"slices"
	"users/common/genproto/users"
	"users/database"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UserService struct {
	db *database.UserDB
	users.UnimplementedUserServiceServer
}

func NewUser() *UserService {
	return &UserService{
		db: database.New(),
	}
}

func (u *UserService) FindByID(ctx context.Context, r *users.UserRequest) (*users.User, error) {
	if r.ID == 0 {
		return nil, status.Error(codes.InvalidArgument, "cannot find user, ID is required")
	}

	for i := 0; i < len(u.db.DB); i++ {
		if u.db.DB[i].ID == r.ID {
			return &u.db.DB[i], nil
		}
	}

	return nil, status.Errorf(codes.NotFound, "cannot find user with ID %d", r.ID)
}

func (u *UserService) Find(ctx context.Context, r *users.UsersRequest) (*users.Users, error) {
	var filtered []*users.User

	for i := 0; i < len(u.db.DB); i++ {
		if r.IDs != nil && !slices.Contains(r.IDs, u.db.DB[i].ID) {
			continue
		}

		if r.FName != nil && u.db.DB[i].FName != *r.FName {
			continue
		}

		if r.City != nil && u.db.DB[i].City != *r.City {
			continue
		}

		if r.Phone != nil && u.db.DB[i].Phone != *r.Phone {
			continue
		}

		if r.Height != nil && u.db.DB[i].Height != *r.Height {
			continue
		}

		if r.Married != nil && u.db.DB[i].Married != *r.Married {
			continue
		}

		filtered = append(filtered, &u.db.DB[i])
	}

	users := &users.Users{
		Users: filtered,
	}
	return users, nil
}
