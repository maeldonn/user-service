package services

import (
	"context"
	"net"
	"testing"
	"users/common/genproto/users"
	"users/database"

	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/test/bufconn"
)

var mockDB = database.UserDB{
	DB: []users.User{
		{
			ID:      1,
			FName:   "John",
			City:    "San Francisco",
			Phone:   "0102030405",
			Height:  5.9,
			Married: true,
		},
		{
			ID:      2,
			FName:   "Jane",
			City:    "New York",
			Phone:   "1112131415",
			Height:  5.3,
			Married: false,
		},
		{
			ID:      3,
			FName:   "Mark",
			City:    "Paris",
			Phone:   "2122232425",
			Height:  6.6,
			Married: true,
		},
	},
}

func NewMockServer(t *testing.T) (users.UserServiceClient, func()) {
	var (
		lis         = bufconn.Listen(1024 * 1024)
		server      = grpc.NewServer()
		userService = &UserService{
			db: &mockDB,
		}
	)

	users.RegisterUserServiceServer(server, userService)
	go func() {
		if err := server.Serve(lis); err != nil {
			t.Errorf("error serving server: %v", err)
		}
	}()

	close := func() {
		err := lis.Close()
		if err != nil {
			t.Errorf("error closing listener: %v", err)
		}
		server.Stop()
	}

	conn, err := grpc.NewClient(
		"passthrough:whatever",
		grpc.WithContextDialer(func(context.Context, string) (net.Conn, error) {
			return lis.Dial()
		}),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		t.Errorf("error connecting to server: %v", err)
	}

	client := users.NewUserServiceClient(conn)
	return client, close
}

func Test_FindByID(t *testing.T) {
	tests := map[string]struct {
		in   *users.UserRequest
		want *users.User

		assertErr assert.ErrorAssertionFunc
		errStr    string
	}{
		"FAIL: no ID specified": {
			in:        &users.UserRequest{},
			assertErr: assert.Error,
			errStr:    "rpc error: code = InvalidArgument desc = cannot find user, ID is required",
		},
		"FAIL: no user found": {
			in: &users.UserRequest{
				ID: 4,
			},
			assertErr: assert.Error,
			errStr:    "rpc error: code = NotFound desc = cannot find user with ID 4",
		},
		"SUCCESS: found a user": {
			in: &users.UserRequest{
				ID: 1,
			},
			want: &users.User{
				ID:      1,
				FName:   "John",
				City:    "San Francisco",
				Phone:   "0102030405",
				Height:  5.9,
				Married: true,
			},
			assertErr: assert.NoError,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			client, close := NewMockServer(t)
			defer close()

			got, err := client.FindByID(context.Background(), tt.in)
			tt.assertErr(t, err)
			if err != nil {
				assert.Equal(t, tt.errStr, err.Error())
				return
			}

			assert.Equal(t, got.ID, tt.want.ID)
			assert.Equal(t, got.FName, tt.want.FName)
			assert.Equal(t, got.City, tt.want.City)
			assert.Equal(t, got.Phone, tt.want.Phone)
			assert.Equal(t, got.Height, tt.want.Height)
			assert.Equal(t, got.Married, tt.want.Married)
		})
	}
}

func Test_Find(t *testing.T) {
	var (
		john        = "John"
		newYork     = "New York"
		phoneNumber = "0102030405"
		tall        = float32(6.6)
		isMarried   = true
	)

	tests := map[string]struct {
		in   *users.UsersRequest
		want []*users.User

		assertErr assert.ErrorAssertionFunc
		errStr    string
	}{
		"SUCCESS: no filter": {
			want: []*users.User{
				{
					ID:      1,
					FName:   "John",
					City:    "San Francisco",
					Phone:   "0102030405",
					Height:  5.9,
					Married: true,
				},
				{
					ID:      2,
					FName:   "Jane",
					City:    "New York",
					Phone:   "1112131415",
					Height:  5.3,
					Married: false,
				},
				{
					ID:      3,
					FName:   "Mark",
					City:    "Paris",
					Phone:   "2122232425",
					Height:  6.6,
					Married: true,
				},
			},
			assertErr: assert.NoError,
		},
		"SUCCESS: filter on IDs": {
			in: &users.UsersRequest{
				IDs: []int32{1, 2},
			},
			want: []*users.User{
				{
					ID:      1,
					FName:   "John",
					City:    "San Francisco",
					Phone:   "0102030405",
					Height:  5.9,
					Married: true,
				},
				{
					ID:      2,
					FName:   "Jane",
					City:    "New York",
					Phone:   "1112131415",
					Height:  5.3,
					Married: false,
				},
			},
			assertErr: assert.NoError,
		},
		"SUCCESS: filter on FName": {
			in: &users.UsersRequest{
				FName: &john,
			},
			want: []*users.User{
				{
					ID:      1,
					FName:   "John",
					City:    "San Francisco",
					Phone:   "0102030405",
					Height:  5.9,
					Married: true,
				},
			},
			assertErr: assert.NoError,
		},
		"SUCCESS: filter on City": {
			in: &users.UsersRequest{
				City: &newYork,
			},
			want: []*users.User{
				{
					ID:      2,
					FName:   "Jane",
					City:    "New York",
					Phone:   "1112131415",
					Height:  5.3,
					Married: false,
				},
			},
			assertErr: assert.NoError,
		},
		"SUCCESS: filter on Phone": {
			in: &users.UsersRequest{
				Phone: &phoneNumber,
			},
			want: []*users.User{
				{
					ID:      1,
					FName:   "John",
					City:    "San Francisco",
					Phone:   "0102030405",
					Height:  5.9,
					Married: true,
				},
			},
			assertErr: assert.NoError,
		},
		"SUCCESS: filter on Height": {
			in: &users.UsersRequest{
				Height: &tall,
			},
			want: []*users.User{
				{
					ID:      3,
					FName:   "Mark",
					City:    "Paris",
					Phone:   "2122232425",
					Height:  6.6,
					Married: true,
				},
			},
			assertErr: assert.NoError,
		},
		"SUCCESS: filter on Married": {
			in: &users.UsersRequest{
				Married: &isMarried,
			},
			want: []*users.User{
				{
					ID:      1,
					FName:   "John",
					City:    "San Francisco",
					Phone:   "0102030405",
					Height:  5.9,
					Married: true,
				},
				{
					ID:      3,
					FName:   "Mark",
					City:    "Paris",
					Phone:   "2122232425",
					Height:  6.6,
					Married: true,
				},
			},
			assertErr: assert.NoError,
		},
		"SUCCESS: filter on all fields, no results": {
			in: &users.UsersRequest{
				IDs:     []int32{1, 2},
				FName:   &john,
				City:    &newYork,
				Phone:   &phoneNumber,
				Height:  &tall,
				Married: &isMarried,
			},
			want:      []*users.User{},
			assertErr: assert.NoError,
		},
		"SUCCESS: filter on multiple fields, results": {
			in: &users.UsersRequest{
				IDs:     []int32{1},
				FName:   &john,
				Phone:   &phoneNumber,
				Married: &isMarried,
			},
			want: []*users.User{
				{
					ID:      1,
					FName:   "John",
					City:    "San Francisco",
					Phone:   "0102030405",
					Height:  5.9,
					Married: true,
				},
			},
			assertErr: assert.NoError,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			client, close := NewMockServer(t)
			defer close()

			got, err := client.Find(context.Background(), tt.in)
			tt.assertErr(t, err)
			if err != nil {
				assert.Equal(t, tt.errStr, err.Error())
				return
			}

			assert.Len(t, got.Users, len(tt.want))

			for i, v := range got.Users {
				assert.Equal(t, v.ID, tt.want[i].ID)
				assert.Equal(t, v.FName, tt.want[i].FName)
				assert.Equal(t, v.City, tt.want[i].City)
				assert.Equal(t, v.Phone, tt.want[i].Phone)
				assert.Equal(t, v.Height, tt.want[i].Height)
				assert.Equal(t, v.Married, tt.want[i].Married)
			}
		})
	}
}
